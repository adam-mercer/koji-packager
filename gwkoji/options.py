# -*- coding: utf-8 -*-
# Copyright(C) 2019 Adam Mercer <adam.mercer@ligo.org>
#
# This file is part of gwkoji.
#
# gwkoji is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

#
# import required modules
#

import argparse
import pathlib

from . import __version__

#
# metadata
#

DEFAULT_TARGET = 'sandbox'


def parser():
    # define option parser
    parser = argparse.ArgumentParser(description="Build RPMs using Koji")
    parser.add_argument(
        '-V', '--version',
        action='version',
        version=__version__,
        help="show version number and exit"
    )
    parser.add_argument(
        '-v', '--verbose',
        action='count',
        default=2,
        help="show verbose output, give once for INFO, give twice for DEBUG"
    )

    # koji options
    koji_options = parser.add_argument_group("Koji Options")
    koji_options.add_argument(
        '--target',
        default=DEFAULT_TARGET,
        help="koji tag to tag (default: %(default)s)"
    )
    koji_options.add_argument(
        '--owner',
        help="koji package owner"
    )
    koji_options.add_argument(
        '--scratch',
        action='store_true',
        default=False,
        help="perform a scratch build (default: %(default)s)"
    )

    # source options
    source_options = parser.add_argument_group("Source Options")
    source_options.add_argument(
        'source',
        type=pathlib.Path,
        help="source to build with Koji"
    )
    source_types = source_options.add_mutually_exclusive_group()
    source_types.add_argument(
        '--tarball',
        action='store_const',
        const='tarball',
        dest='source_type',
        help="source is a tarball"
    )
    source_types.add_argument(
        '--git',
        action='store_const',
        const='git',
        dest='source_type',
        help="source is a git repo"
    )
    source_types.add_argument(
        '--src-rpm',
        action='store_const',
        const='srcrpm',
        dest='source_type',
        help="source is a src.rpm"
    )
    source_types.add_argument(
        '--spec',
        action='store_const',
        const='spec',
        dest='source_type',
        help="source is an RPM spec file"
    )
    return parser


def parse_options():
    return parser().parse_args()
