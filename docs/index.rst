#################
 GWKoji Packager
#################

============
Installation
============

Installation requires `python>=3.6`

.. code-block:: bash

   $ python -m pip install git+https://git.ligo.org/packaging/koji-packager.git

==================
User documentation
==================

.. toctree::
   :maxdepth: 2

   cli

==================
API documentaation
==================

.. include:: gwkoji.rst

==================
Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
