# -*- coding: utf-8 -*-
# Copyright(C) 2019 Adam Mercer <adam.mercer@ligo.org>
#
# This file is part of gwkoji.
#
# gwkoji is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

#
# import required modules
#

import atexit
import logging
import shutil
import subprocess
import sys
import tempfile

from . import (
    koji,
    options,
    rpm,
)
from .build import build_src_rpm
from .utils import source_type

#
# setup logger
#

LOGGER_FORMAT = "[%(asctime)-15s] %(name)s | %(levelname)+8s | %(message)s"
LOGGER_DATEFMT = "%Y-%m-%d %H:%M:%S"

logging.basicConfig(format=LOGGER_FORMAT, datefmt=LOGGER_DATEFMT)
logger = logging.getLogger('gwkoji')
try:
    import coloredlogs
except ImportError:
    set_logger_level = logger.setLevel
else:
    coloredlogs.install(
        logger=logger,
        fmt=LOGGER_FORMAT,
        datefmt=LOGGER_DATEFMT,
    )
    set_logger_level = coloredlogs.set_level


#
# main program
#

def main():
    args = options.parse_options()

    # set verbosity
    set_logger_level(10 * max(5 - args.verbose, 0))

    # print info
    logger.info("-- gwkoji-packager -----")
    logger.info("source = %s" % args.source)
    logger.info("target tag = %s" % args.target)

    # determine source type
    if not args.source_type:
        args.source_type = source_type(args.source)
    logger.info("source type = {!r}".format(args.source_type))

    # check that source exists
    if args.source_type != 'git' and not args.source.is_file():
        raise FileNotFoundError(
            "No such file or directory: '{0}'".format(args.source),
        )

    if args.source_type not in ('srcrpm', 'git'):
        logger.info("generating src.rpm...")
        tmpdir = tempfile.mkdtemp()
        atexit.register(shutil.rmtree, tmpdir)
        args.source = build_src_rpm(args.source, outdir=tmpdir, logger=logger)
        logger.info("temporary src.rpm generated as '{0}'".format(args.source))

    # get pkg name from source rpm
    try:
        if args.source_type != 'git':
            pkg_name = rpm.get_package_name_from_source_rpm(
                str(args.source),
                logger=logger
            )
            logger.info("package name = %s" % pkg_name)
        # how do we determine this when source is from git?
    except subprocess.CalledProcessError as e:
        logger.critical("unable to determine package name, %s" % e)
        raise

    # check that we can authenticate to koji
    try:
        koji.moshimoshi(logger=logger)
    except subprocess.CalledProcessError:
        logger.critical("unable to authenticate with koji server")
        raise

    # determine if the package is already registered with koji
    if args.source_type != 'git':
        try:
            koji.list_pkgs('--quiet', '--package', pkg_name, logger=logger)
        except subprocess.CalledProcessError:
            # register package with koji
            if args.owner is None:
                logger.critical("owner not specified")
                sys.exit(1)
            try:
                koji.add_pkg(args.owner, pkg_name, logger=logger)
            except subprocess.CalledProcessError as e:
                logger.critical("unable to regsiter package: %s" % e)
                raise

    # attempt to build package
    opts = [args.target, str(args.source)]
    if args.scratch:
        opts.insert(0, '--scratch')
    try:
        koji.build(*opts)
    except subprocess.CalledProcessError as e:
        logger.critical("unable to build package: %s" % e)
        raise

    logger.info("-- Complete -----")


#
# main program entry point
#

if __name__ == '__main__':
    main()
