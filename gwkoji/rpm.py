# -*- coding: utf-8 -*-
# Copyright(C) 2019 Adam Mercer <adam.mercer@ligo.org>
#
# This file is part of gwkoji.
#
# gwkoji is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

"""Interactions with rpm
"""

from distutils.spawn import find_executable

from .utils import logged_check_output


# method to get the package name from the source
def get_package_name_from_source_rpm(*args, **kwargs):
    rpm = find_executable('rpm')
    if rpm is None:
        raise FileNotFoundError("No such file or directory: 'rpm'")
    cmd = (rpm, '--queryformat', '%{NAME}', '-qp',) + args
    return logged_check_output(cmd, **kwargs).decode()
