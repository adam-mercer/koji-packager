%define name    gwkoji
%define version 0.1.0
%define release 1

%if 0%{?python3_version_nodots} < 36
%define __python3 %{__python3_other}
%define python3_version %{python3_other_version}
%define python3_version_nodots %{python3_other_version_nodots}
%define py3_build %{py3_other_build}
%define py3_install %{py3_other_install}
%define python3_sitelib %{python3_other_sitelib}
%endif

Name:      %{name}
Version:   %{version}
Release:   %{release}%{?dist}
Summary:   Python library for interacting with the Koji RPM build system

Group:     Development/Libraries
License:   GPL-3.0-or-later
Url:       https://git.ligo.org/adam-mercer/koji-packager/
Source0:   %{name}-%{version}.tar.gz
Packager:  Duncan Macleod <duncan.macleod@ligo.org>

BuildArch: noarch

# build dependencies
BuildRequires: rpm-build
BuildRequires: python-rpm-macros
BuildRequires: python3-rpm-macros
BuildRequires: python%{python3_version_nodots}-setuptools
BuildRequires: help2man

# testing dependencies (python3x only)
BuildRequires: python%{python3_version_nodots}-pytest >= 2.8.0

%description
FIXME

# -- python3x-gwkoji

%package -n python%{python3_version_nodots}-%{name}
Summary: Python %{python3_version} library for interacting with the Koji RPM build system
Requires: koji
Requires: krb5-workstation
Requires: python%{python3_version_nodots}-setuptools
Requires: rpm-build
Requires: rpmdevtools
%{?python_provide:%python_provide python%{python3_version_nodots}-%{name}}
%description -n python%{python3_version_nodots}-%{name}
FIXME

# -- build steps

%prep
%autosetup -n %{name}-%{version}

%build
%py3_build

%install
%py3_install
# make man page for gwkoji-packager
mkdir -vp %{buildroot}%{_mandir}/man1
env PYTHONPATH="%{buildroot}%{python3_sitelib}" \
help2man \
    --source %{name} \
    --version-string %{version} \
    --section 1 --no-info --no-discard-stderr \
    --output %{buildroot}%{_mandir}/man1/gwkoji-packager.1 \
    %{buildroot}%{_bindir}/gwkoji-packager

%check
%{__python3} -m pytest --pyargs %{name}
%{__python3} -m gwkoji.packager --help
PYTHONPATH="%{buildroot}%{python3_sitelib}" %{buildroot}%{_bindir}/gwkoji-packager --help

%files -n python%{python3_version_nodots}-%{name}
%doc README.rst
%license COPYING
%{python3_sitelib}/*
%{_bindir}/gwkoji-packager
%{_mandir}/man1/gwkoji-packager.1*

# -- changelog

%changelog
* Tue May 14 2019 Duncan Macleod <duncan.macleod@ligo.org> - 0.1.0-1
- first cut at packaging gwkoji
