# -*- coding: utf-8 -*-
# Copyright(C) 2019 Adam Mercer <adam.mercer@ligo.org>
#
# This file is part of gwkoji.
#
# gwkoji is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

"""Test suite for gwkoji.rpm
"""

import pytest
from unittest import mock

from .. import rpm as gwkoji_rpm


@mock.patch('gwkoji.rpm.find_executable', return_value='/usr/bin/rpm')
@mock.patch('gwkoji.rpm.logged_check_output')
def test_download_spec_sources(call, _):
    gwkoji_rpm.get_package_name_from_source_rpm('foobar-1.0-1.el7', logger=1)
    call.assert_called_once_with(
        ('/usr/bin/rpm', '--queryformat', '%{NAME}', '-qp',
         'foobar-1.0-1.el7'), logger=1,
    )


@mock.patch('gwkoji.rpm.find_executable', return_value=None)
def test_function_errors(_):
    with pytest.raises(FileNotFoundError):
        gwkoji_rpm.get_package_name_from_source_rpm(
                'foobar-1.0-1.el7',
                logger=1
        )
