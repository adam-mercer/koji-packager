# -*- coding: utf-8 -*-
# Copyright(C) 2019 Adam Mercer <adam.mercer@ligo.org>
#
# This file is part of gwkoji.
#
# gwkoji is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

import pytest
from unittest import mock

from .. import koji as gwkoji_koji


@mock.patch('gwkoji.koji.find_executable', return_value=None)
def test_moshimoshi_errors(_):
    with pytest.raises(FileNotFoundError):
        gwkoji_koji.moshimoshi(logger=1)


@mock.patch('gwkoji.koji.find_executable', return_value='/usr/bin/koji')
@mock.patch('gwkoji.koji.logged_check_call')
def test_moshimoshi(call, _):
    gwkoji_koji.moshimoshi(logger=1)
    call.assert_called_once_with(('/usr/bin/koji', 'moshimoshi'), logger=1)


@mock.patch('gwkoji.koji.find_executable', return_value='/usr/bin/koji')
@mock.patch('gwkoji.koji.logged_check_call')
def test_list_pkgs(call, _):
    gwkoji_koji.list_pkgs('--quiet', '--package', 'foobar', logger=1)
    call.assert_called_once_with(
        ('/usr/bin/koji', 'list-pkgs', '--quiet', '--package', 'foobar'),
        logger=1
    )


@mock.patch('gwkoji.koji.find_executable', return_value='/usr/bin/koji')
@mock.patch('gwkoji.koji.logged_check_call')
def test_add_pkg(call, _):
    gwkoji_koji.add_pkg('albert.einstein@LIGO.ORG', 'foobar', logger=1)
    call.assert_called_once_with(
        ('/usr/bin/koji', 'add-pkg', '--owner',
         'albert.einstein@LIGO.ORG', 'foobar'), logger=1
    )


@mock.patch('gwkoji.koji.find_executable', return_value='/usr/bin/koji')
@mock.patch('gwkoji.koji.logged_check_call')
def test_build(call, _):
    gwkoji_koji.build('sandbox', 'foobar-1.0-1.el7.src.rpm', logger=1)
    call.assert_called_once_with(
        ('/usr/bin/koji', 'build', 'sandbox', 'foobar-1.0-1.el7.src.rpm'),
        logger=1
    )
