# -*- coding: utf-8 -*-
# Copyright(C) 2019 Adam Mercer <adam.mercer@ligo.org>
#
# This file is part of gwkoji.
#
# gwkoji is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

import os.path
import pathlib
import re
import shutil
import tempfile
from distutils.spawn import find_executable

from .utils import (
    logged_check_call,
    logged_check_output,
    source_type,
)

RPMBUILD_TS_RESP = re.compile(r'\AWrote: (?P<srcrpm>\S+)\Z')


def _rpmbuild_args(*args, tmpdir=None):
    """Construct a command-line call to ``rpmbuild``
    """
    rpmbargs = [find_executable('rpmbuild'), '--verbose']
    if tmpdir:
        rpmbargs.extend(('--define', '_topdir {0}'.format(tmpdir)))
    return rpmbargs + list(args)


def download_spec_sources(spec, outdir, logger=None):
    """Run spectool to download sources defined in a spec file

    Parameters
    ----------
    spec : `str`, `pathlib.Path`
        path to spec file

    outdir : `str`, `pathlib.Path`
        output directory for downloaded files
    """
    spectool = find_executable('spectool')
    cmd = [
        spectool,
        '--get-files',
        '--directory',
        str(outdir),
        str(spec),
    ]
    return logged_check_call(
        cmd,
        logger=logger,
    )


def build_src_rpm(source, *extra_rpmbuild_args, outdir=os.path.curdir,
                  logger=None):
    """Create a src RPM from a tarball or a spec file

    Parameters
    ----------
    source : `str`, `pathlib.Path`
        path to source file, either a tarball or a spec file

    *extra_rpmbuild_args
        positional arguments are passed as command-line arguments to
        ``rpmbuild``

    outdir : `str`, `pathlib.Path`, optional
        output directory for ``src.rpm`` file

    Returns
    -------
    srcrpm : `str`
        the path of the new `src.rpm` file
    """
    type_ = source_type(source)

    with tempfile.TemporaryDirectory() as tmpdir:
        tmppath = pathlib.Path(tmpdir)
        sourcepath = tmppath / 'SOURCES'
        sourcepath.mkdir()

        # run spectool to download sources
        if type_ == 'spec':
            download_spec_sources(source, sourcepath, logger=logger)

        # run rpmbuild
        rpmcmd = '-bs' if type_ == 'spec' else '-ts'
        cmd = _rpmbuild_args(*extra_rpmbuild_args + (rpmcmd, str(source)),
                             tmpdir=tmpdir)
        resp = logged_check_output(cmd, logger=logger).strip().decode('utf-8')

        # copy src.rpm into outdir and return path
        srcrpm = RPMBUILD_TS_RESP.match(resp).groupdict()['srcrpm']
        return shutil.move(srcrpm, outdir)
