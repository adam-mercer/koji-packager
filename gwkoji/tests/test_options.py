# -*- coding: utf-8 -*-
# Copyright(C) 2019 Adam Mercer <adam.mercer@ligo.org>
#
# This file is part of gwkoji.
#
# gwkoji is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

"""Test suite for gwkoji.packager
"""

import sys
from pathlib import Path
from unittest import mock

import pytest

from .. import options as gwkoji_options


@mock.patch.object(sys, 'argv', ['gwkoji-packager', 'test.spec', '--spec'])
def test_parse_options():
    args = gwkoji_options.parse_options()
    assert args.source == Path('test.spec')
    assert args.source_type == 'spec'


@mock.patch.object(sys, 'argv',
                   ['gwkoji-packager', 'test.spec', '--spec', '--tarball'])
def test_parse_options_exclusive_source_type():
    with pytest.raises(SystemExit):
        gwkoji_options.parse_options()
