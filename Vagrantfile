Vagrant.configure(2) do |config|
  config.vm.define "koji-packager"
  config.vm.box = "centos/7"
  config.vm.network "private_network", type: "dhcp"
  # configure extra repositories
  config.vm.provision "file", source: "environment/yum.repos.d", destination: "/tmp/yum.repos.d"
  config.vm.provision "shell",
    inline: "mv /tmp/yum.repos.d/*.repo /etc/yum.repos.d/ && rm -r /tmp/yum.repos.d"
  config.vm.provision "shell", inline: <<-SHELL
    echo "koji.vagrant" > /etc/hostname
    # add upstream epel repository
    yum -y install epel-release

    # install osg repository
    rpm -Uvh https://repo.opensciencegrid.org/osg/3.4/osg-3.4-el7-release-latest.rpm
    yum clean all

    # add git-lfs repository
    curl -s https://packagecloud.io/install/repositories/github/git-lfs/script.rpm.sh | bash

    # install required dependencies
    yum -y install \
      deltarpm \
      emacs \
      git-lfs \
      git2u \
      koji \
      krb5-workstation \
      ldg-client \
      less \
      mock \
      python36 \
      python36-pytest \
      python36-pytest-cov \
      rpm-build \
      rpmdevtools \
      rpmlint \
      sudo \
      vim \
      wget
  SHELL
  # configure koji, kerberos, and mock
  config.vm.provision "file", source: "environment/koji.conf", destination: "/tmp/koji.conf"
  config.vm.provision "file", source: "environment/krb5.conf", destination: "/tmp/krb5.conf"
  config.vm.provision "file", source: "environment/mock/.", destination: "/tmp/mock"
  config.vm.provision "shell",
    inline: "mv /tmp/{koji,krb5}.conf /etc && mv /tmp/mock/*.cfg /etc/mock && rm -r /tmp/mock"
end
