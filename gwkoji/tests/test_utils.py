# -*- coding: utf-8 -*-
# Copyright(C) 2019 Adam Mercer <adam.mercer@ligo.org>
#
# This file is part of gwkoji.
#
# gwkoji is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

"""Test suite for gwkoji.utils
"""

import pytest

from .. import utils as gwkoji_utils


@pytest.mark.parametrize("source, type_", [
    ('git+https://git.ligo.org/albert/gr#origin/rhel', 'git'),
    ('mypackage.spec', 'spec'),
    ('mypackage.src.rpm', 'srcrpm'),
    ('mypackage.tar', 'tarball'),
    ('mypackage.tar.bz2', 'tarball'),
    ('mypackage.tar.gz', 'tarball'),
    ('mypackage.tar.xz', 'tarball'),
])
def test_source_type(source, type_):
    assert gwkoji_utils.source_type(source) == type_


def test_source_type_error():
    with pytest.raises(ValueError):
        gwkoji_utils.source_type('blah')
